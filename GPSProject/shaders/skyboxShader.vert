#version 410 core

layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec3 vNormal;

out vec3 textureCoordinates;
out vec3 Normal;
out vec3 Position;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;


void main()
{
	Normal = mat3(transpose(inverse(model))) * vNormal;
    Position = vec3(model * vec4(vertexPosition, 1.0));

    vec4 tempPos = projection * view * vec4(vertexPosition, 1.0);
    gl_Position = tempPos.xyww;
    textureCoordinates = vertexPosition;
}
